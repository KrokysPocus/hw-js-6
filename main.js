// Відповіді на запитання:
//
// 1. Для того, щоб позначити певний символ, як частину рядку, який без особливого позначення може бути сприйнятий
//    як метасимвол та не відобразитися як потрібно.
// 2. За допомогою команди function, через змінну, а також стрілкову функцію(=>).
// 3. Це концепція JS, при якій декларування змінних всередині функцій буду відбуватисб навіть до їх ініціалізації
//    певним значенням.



function createNewUser(newUser) {
    newUser = {
        firstName: prompt("Enter your name"),
        lastName: prompt("Enter your surname"),
        birthDate: prompt("Enter your birth date", "dd.mm.yyyy"),
        getLogin(generateUsername) {
            generateUsername = Array(this.firstName[0]) + this.lastName;
            return generateUsername.toLocaleLowerCase();
        },
        getAge(generateAge) {
            let now = new Date();
            generateAge = now.getFullYear() - this.birthDate.substring(6, 10)
            let month = now.getMonth() - this.birthDate.substring(3, 5)
            if (month < 0 || (month === 0 && now.getDate() < this.birthDate.substring(3, 5))) {
                generateAge--;
            }

            return generateAge;
        },
        getPassword(generatePassword) {
            generatePassword = Array(this.firstName[0].toUpperCase()) + this.lastName.toLowerCase() + this.birthDate.substring(6, 10);
            return generatePassword;
        }
    }
    return newUser;
}

const user = createNewUser()

console.log(`Your username is ${user.getLogin()}`)
console.log(`You are ${user.getAge()} years old`)
console.log(`Your password is ${user.getPassword()}`)
